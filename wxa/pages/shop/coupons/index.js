const app = getApp();
Page({
    data: {
        myCoupons: ["未使用", "已使用", "已过期"], //优惠卷菜单列表
        color: wx.getExtConfigSync().color,
        coupon_list: [],
        status: 0
    },
    onShow() {
        this.couponsSearch();
    },
    // 选择优惠券类型
    couponsSearch(e) {
        const index = e ? e.currentTarget.dataset.index : 0;
        this.setData({
            couponTabIndex: index
        });
        this.couponList(index==0 ? 0 : index==1 ? 1 : -1);
    },
    // 优惠券列表
    couponList(status) {
        wx.doodoo.fetch(`/shop/api/shop/coupon/userCoupon?status=${status}`).then(res => {
            if (res && res.data.errmsg == 'ok') {
                this.data.coupon_list = res.data.data;
                for (const menu of this.data.coupon_list) {
                    menu.coupon.last_time = menu.coupon.ended_at.substr(0, 10);
                    menu.coupon.start_time = menu.coupon.started_at.substr(0, 10);
                }
                let length = 2; // 默认设置两个数字宽度 22 50 86 75
                if (this.data.coupon_list.length) {
                    this.data.coupon_list.sort((a, b) => {
                        if (a.coupon && a.coupon.price && b.coupon && b.coupon.price) {
                            return a.coupon.price < b.coupon.price; // 从大到小排列
                        }
                    })
                    length = String(this.data.coupon_list[0].coupon.price).length;
                }
                this.setData({
                    status: status,
                    length: length,
                    coupon_list: this.data.coupon_list
                });
            }
        });
    },
    // 使用优惠券
    use() {
        wx.switchTab({
            url: "/pages/index/index"
        });
    },
    onPullDownRefresh: function() {
        setTimeout(() => {
            wx.stopPullDownRefresh();
            this.data.coupon_list = [];
            this.couponsSearch();
        }, 600)
    }
});