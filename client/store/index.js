import Vuex from "vuex";

function getCoryright(host) {
    if (host === "127.0.0.1" || host === "localhost" || host === "0.0.0.0") {
        return host;
    }

    let hosts = host.split(".");
    hosts.shift();
    return hosts.join(".");
}

function isAgent(host) {
    const copyright = getCoryright(host);
    if (copyright.split(":")[0] === "localhost") {
        return false;
    }
    return true;
}

const createStore = () => {
    return new Vuex.Store({
        state: {
            agent: false,
            host: "",
            custom: {},
            copyright: "localhost",
            componentId: 0,
            color: "#000000",
            pageId: 0
        },
        mutations: {
            SET_AGENT(state, agent) {
                state.agent = agent;
            },
            SET_CUSTOM(state, custom) {
                state.custom = custom;
            },
            SET_HOST(state, host) {
                state.host = host;
            },
            SET_CORYRIGHT(state, copyright) {
                state.copyright = copyright;
            },
            SET_COMPONENT_ID(state, id) {
                state.componentId = id;
            },
            SET_PAGE_ID(state, id) {
                state.pageId = id;
            },
            SET_COLOR(state, color) {
                state.color = color;
            }
        },
        actions: {
            nuxtServerInit({ commit }, { app, req }) {
                const xHost = req.headers["x-host"];
                if (xHost) {
                    commit("SET_AGENT", isAgent(xHost));
                    commit("SET_HOST", xHost);
                    commit("SET_CORYRIGHT", getCoryright(xHost));
                }
                if (app.$cookies.get("custom")) {
                    commit("SET_CUSTOM", app.$cookies.get("custom"));
                }
            }
        }
    });
};

export default createStore;
